local set = vim.opt

-- Blink cursor :D
vim.cmd[[set guicursor+=a:-blinkwait175-blinkoff150-blinkon175]]

set.expandtab = true
set.smarttab = true
set.shiftwidth = 2
set.tabstop = 2

set.hlsearch = false
set.ignorecase = true
set.smartcase = true

set.splitbelow = true
set.splitright = true
set.wrap = true
set.scrolloff = 5
set.fileencoding = "utf-8"
set.termguicolors = true
set.cmdheight = 1

set.number = true
set.relativenumber = true
set.cursorline = true

set.hidden = true
set.showmode = false
set.showtabline = 2
set.signcolumn = "yes"
set.mouse = "a"

set.undofile = true
set.swapfile = false
set.backup = false
set.clipboard = "unnamedplus"
set.completeopt = "menuone,noselect"
set.colorcolumn = "81"
set.conceallevel = 2
